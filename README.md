# AWS Angular

By Levan Roinishvili

Build Angular projects and deploy them to AWS S3



## Installed Software

* **Linux** Version [Alpine Linux](http://alpinelinux.org) v3.6 (Kernel 4.9.93-linuxkit-aufs)
* **Node.js** Version 8.11.4
* **Python** Version 2.7.14
* **AWS CLI** Version 1.15.81


## Sample `bitbucket-pipelines.yml` file

```YAML
# Build an Angular app and publish it to AWS S3 bucket

image: levanroinishvili/aws-angular:6.0.2


pipelines:
  branches:
    master:
      - step:
          caches:
            - node
            - pip
          script:
            - npm install # Install dependences for your Angular app
            - npm run build # Build your Angular app

            # Set AWS credentials
            # It is more secure to do this via Bitbucket's Environment variables
            # (Bitbucket -> Your Repo -> Settings -> Environment variables)
            - export AWS_ACCESS_KEY_ID="Your AWS Access Key ID - in Clear Text - Bad"
            - export AWS_SECRET_ACCESS_KEY="Your AWS Secret Access key - in Clear Text - Bad"
            # - export AWS_ACCESS_KEY_ID="$AWS_ACCESS_KEY_ID"
            # - export AWS_SECRET_ACCESS_KEY="$AWS_SECRET_ACCESS_KEY"


            # Delete old files on AWS S3 bucket
            - aws s3 sync --delete ./dist/verysmart s3://very-smart-systems --acl public-read --cache-control no-cache

            # - bash ./brotli-compression.sh

            # Copy new files to AWS S3 bucket
            - aws s3 cp ./dist/verysmart s3://very-smart-systems --acl public-read --recursive --exclude "*" --include "*.br" --content-encoding "br" --cache-control no-cache


            # - bash ./gzip-compression.sh
            # - aws s3 cp ./dist/verysmart s3://very-smart-systems --acl public-read --recursive --exclude "*" --include "*.gz" --content-encoding "gzip" --cache-control no-cache

```

### Temporary Problems

Recently a problem was noticed whith installing [node-sass](https://www.npmjs.com/package/node-sass)
when the container is running on BitBucket.

To avoid this problem, a temporary addition was made to the image. It now includes folder `/node_modules`,
which containes pre-installed `node-sass` and its dependencies.

This feature can be used by making a small change to the `bitbucket-pipelines.yml` file:
If `node-sass` is not already installed (through BitBucket cache), then copy pre-installed modules to `node_modules` and rebuild `node-sass`.

```YAML
pipelines:
  branches:
    master:
      - step:
          script:
            # If node-sass is not installed (through BitBucket cache), copy pre-installed node-sass modules
            - if [ ! -e /opt/atlassian/pipelines/agent/build/node_modules/node-sass ]; then mkdir -p /opt/atlassian/pipelines/agent/build/node_modules; mv /node_modules/* /opt/atlassian/pipelines/agent/build/node_modules; npm rebuild node-sass; fi
            # - mkdir -p /opt/atlassian/pipelines/agent/build/node_modules
            # - mv /node_modules/* /opt/atlassian/pipelines/agent/build/node_modules
            # - npm rebuild node-sass
```
