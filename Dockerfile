# By Levan Roinishvili
# Build Angular apps with Angular CLI and deploy them to AWS S3

FROM node:10.12-alpine

# -------------------------- BEGIN: Install AWS CLI -----------------------
# Set path to aws
# ENV PATH /root/.local/bin:$PATH

RUN apk add --no-cache \
        python2 \
        py-pip \
    && pip install --upgrade pip \
    && pip install awscli

# RUN mkdir /node_cache \
#     && cd /node_cache \
#     && npm install node-sass \
#     && rm /node_cache/package-lock.json \
#     && mv /node_cache/node_modules /node_modules \
#     && rm -rf /node_cache

CMD ["/bin/sh"]
